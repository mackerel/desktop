# Copyright 2012 Benedikt Morbach <benedikt.morbach@googlemail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hughsie ] \
    meson \
    python [ blacklist=2 multibuild=false with_opt=true option_name=gobject-introspection ] \
    vala [ with_opt=true option_name=vapi vala_dep=true ]

SUMMARY="GObject wrapper for libusb1"
DOWNLOADS="https://people.freedesktop.org/~hughsient/releases/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# tests try to access usb devices
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libusb:1[>=1.0.22]
        gobject-introspection? (
            dev-python/setuptools:*[python_abis:*(-)?] [[ note = [ for pkg_resources ] ]]
            gnome-desktop/gobject-introspection:1[>=1.29][python_abis:*(-)?]
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dusb_ids=/usr/share/misc/usb.ids
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc docs'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_prepare() {
    meson_src_prepare

    # Respect selected python abi
    if option gobject-introspection ; then
        edo sed \
            -e "s:pymod.find_installation():pymod.find_installation('python$(python_get_abi)'):g" \
            -i gusb/meson.build
    fi
}

