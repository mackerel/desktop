# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github cmake python [ blacklist=2 multibuild=false with_opt=true ]

SUMMARY="A library that provides automatic proxy configuration management"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gnome [[ description = [ Add support for using GNOME3 settings to determine the correct proxy ] ]]
    kde [[ description = [ Add support for using KDE settings to determine the correct proxy ] ]]
    networkmanager [[ description = [ Add support for using NetworkManager to check if the network topology changed ] ]]
    pacrunner [[ description = [ Add support for querying the PACrunner daemon to determine proxy settings ] ]]
    webkit [[ description = [ Use the webkit javascript engine for PAC ] ]]
    xulrunner [[ description = [ Use the xulrunner javascript engine for PAC ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        gnome? ( dev-libs/glib:2[>=2.26] )
        networkmanager? (
            net-apps/NetworkManager
            sys-apps/dbus
        )
        pacrunner? ( sys-apps/dbus )
        webkit? ( net-libs/webkit:= )
        xulrunner? ( dev-libs/spidermonkey:68 )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSHARE_INSTALL_DIR:PATH=/usr/share
    -DWITH_DOTNET:BOOL=FALSE
    -DWITH_GNOME2:BOOL=FALSE
    -DWITH_PERL:BOOL=FALSE
    -DWITH_PYTHON2:BOOL=FALSE
    -DWITH_VALA:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'python PYTHON3'
    'gnome GNOME3'
    'kde KDE'
    'networkmanager NM'
    'pacrunner DBUS'
    'webkit WEBKIT'
    'webkit WEBKIT3'
    'xulrunner MOZJS'
)

src_configure() {
    local cmakeparams=(
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTION_WITHS[@]}" ; do \
            cmake_with ${s} ; \
        done )
    )

    if option python ; then
        cmakeparams+=(
            -DPYTHON3_SITEPKG_DIR:PATH=$(python_get_sitedir)
        )
    fi

    ecmake "${cmakeparams[@]}"
}

