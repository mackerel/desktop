# Copyright 2020-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

ROBIN_HOOD_HASHING_REV="3.11.3"

require github [ user=KhronosGroup pn=Vulkan-ValidationLayers tag=v${PV} ] \
    cmake

SUMMARY="Vulkan Validation Layers"
DESCRIPTION="
Vulkan is an Explicit API, enabling direct control over how GPUs actually work. By design, minimal
error checking is done inside a Vulkan driver. Applications have full control and responsibility
for correct operation. Any errors in how Vulkan is used can result in a crash. This project
provides Vulkan validation layers that can be enabled to assist development by enabling developers
to verify their applications correct use of the Vulkan API.
"
HOMEPAGE+=" https://www.khronos.org/vulkan"
DOWNLOADS+="
    https://github.com/martinus/robin-hood-hashing/archive/${ROBIN_HOOD_HASHING_REV}.tar.gz -> robin-hood-hashing-${ROBIN_HOOD_HASHING_REV}.tar.gz
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    X
    wayland
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-lang/spirv-tools[>=2021.4]
        sys-libs/spirv-headers[>=1.5.4-r4]
        sys-libs/vulkan-headers
        virtual/pkg-config
        X? (
            x11-libs/libxcb
            x11-libs/libX11
            x11-libs/libXrandr
        )
        wayland? ( sys-libs/wayland )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.2.131-fix-shared.patch
    "${FILES}"/f6623d073e771fb16855b89233badc5781ad3abc.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_LAYER_SUPPORT_FILES:BOOL=TRUE
    -DBUILD_LAYERS:BOOL=TRUE
    -DBUILD_TESTS:BOOL=FALSE
    -DBUILD_WERROR:BOOL=FALSE
    -DCMAKE_INSTALL_INCLUDEDIR:PATH=/usr/$(exhost --target)/include
    -DINSTRUMENT_OPTICK:BOOL=FALSE
    -DROBIN_HOOD_HASHING_INSTALL_DIR:PATH="${WORKBASE}"/robin-hood-hashing-${ROBIN_HOOD_HASHING_REV}
    -DSPIRV_HEADERS_INSTALL_DIR:PATH=/usr/$(exhost --target)
    -DSPIRV_TOOLS_INSTALL_DIR:PATH=/usr/$(exhost --target)
    -DUPDATE_DEPS:BOOL=FALSE
    -DUSE_CCACHE:BOOL=FALSE
    -DUSE_ROBIN_HOOD_HASHING:BOOL=TRUE
    -DVulkanRegistry_DIR:PATH=/usr/share/vulkan/registry
    -DVVL_ENABLE_ASAN:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    "X WSI_XCB_SUPPORT"
    "X WSI_XLIB_SUPPORT"
    "wayland WSI_WAYLAND_SUPPORT"
)

